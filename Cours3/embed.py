#!/usr/bin/env python3
from random import randint

import numpy as np
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import pandas as pd
from torch.nn import Parameter

from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


corpus = [
    'warsaw is poland capital',
    'berlin is germany capital',
    'paris is france capital',
    'Madrid is spain capital',
    'Roma is italy capital',
    'london is united_kingdom capital',
    'Moscow is russia capital',
    'tokyo is japan capital',
    'beijing is china capital'
]


def tokenize_corpus(corpus):
    tokens = [x.split() for x in corpus]
    return tokens


def get_input_layer(word_idx):
    x = torch.zeros(vocabulary_size).float()
    x[word_idx] = 1.0
    return x


tokenized_corpus = tokenize_corpus(corpus)

vocabulary = []
idx_pairs = []
window_size = 2
embedding_dims = 2
num_epochs = 1000
learning_rate = 0.05
loss_val = 0

for token in tokenized_corpus:
    for word in token:
        if word not in vocabulary:
            vocabulary.append(word)


word2idx = {w: idx for (idx, w) in enumerate(vocabulary)}
idx2word = {idx: w for (idx, w) in enumerate(vocabulary)}

vocabulary_size = len(vocabulary)

for sentence in tokenized_corpus:
    indices = [word2idx[word] for word in sentence]
    for center_word_pos in range(len(indices)):
        for w in range(-window_size, window_size + 1):
            context_word_pos = center_word_pos + w
            if context_word_pos < 0 or context_word_pos >= len(indices) or center_word_pos == context_word_pos:
                continue
            context_word_idx = indices[context_word_pos]
            idx_pairs.append((indices[center_word_pos], context_word_idx))

idx_pairs = np.array(idx_pairs)


W1 = Variable(torch.randn(embedding_dims, vocabulary_size).float(), requires_grad=True)
W2 = Variable(torch.randn(vocabulary_size, embedding_dims).float(), requires_grad=True)

loss_val = 0

for i in range(0, num_epochs):
    for data, target in idx_pairs:
        x = Variable(get_input_layer(data)).float()
        y_true = Variable(torch.from_numpy(np.array([target])).long())

        z1 = torch.matmul(W1, x)
        z2 = torch.matmul(W2, z1)

        log_softmax = F.log_softmax(z2, dim=0)

        loss = F.nll_loss(log_softmax.view(1, -1), y_true)
        loss_val += loss.data[0]
        loss.backward()
        W1.data -= learning_rate * W1.grad.data
        W2.data -= learning_rate * W2.grad.data

        W1.grad.data.zero_()
        W2.grad.data.zero_()

        # if i % 10 == 0:
        #     print(f'Loss at epo {i}: {loss_val/len(idx_pairs)}')

        res = W1.detach().numpy().transpose()
res = pd.DataFrame(res)


plt.scatter(res[0], res[1])
if vocabulary:
    for i, word in enumerate(vocabulary):
        plt.annotate(word,
                     (res[0][i],
                      res[1][i]))
plt.show()

