#!/usr/bin/env python3
import spacy
import os

nlp = spacy.load('en_core_web_sm')
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS
with open('txt_sentoken/pos/cv000_29590.txt', 'r') as input_file:
    doc = nlp(input_file.read())
    with open('txt_sentoken_corrected/pos/' + 'cv000_29590.txt'[:-4] + '_corrected.txt', 'w+') as output_file:
        tokens = [token for token in doc if (token.text not in spacy_stopwords) or (not token.is_punct)]
        for token in tokens:
            if not token.is_punct:
                if token.text != ' ':
                    if token.text != '\n':
                        print(token.text)
