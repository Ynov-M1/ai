#!/usr/bin/env python3
import spacy

nlp = spacy.load('en_core_web_sm')
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS

with open('spacy_txt.txt', 'r') as file:
    doc = nlp(file.read())
    with open('spacy_parse.csv', 'w+') as output_file:
        output_file.write('text,lemma,pos\n')
        tokens = [token for token in doc if token.text not in spacy_stopwords]
        print(tokens)
        for token in tokens:
            token_text = token.text
            token_lemma = token.lemma_

            if token_text == ',':
                token_text = '\",\"'
                token_lemma = '\",\"'
            line = token_text + ',' + token_lemma + ',' + token.pos_ + '\n'
            output_file.write(line)
