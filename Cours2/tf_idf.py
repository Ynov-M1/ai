#!/usr/bin/env python3
import spacy
import os

term_frequency = []
term_frequency_by_txt = []
term_frequency_for_all_txt = []
onlyfiles_pos = [name for name in os.listdir('txt_sentoken_corrected/pos/') if name[-4:] == ".txt"]
# onlyfiles_neg = [name for name in os.listdir('txt_sentoken_corrected/neg/') if name[-4:] == ".txt"]


nlp = spacy.load('en_core_web_sm')

# get words occurences by text
for file in onlyfiles_pos[:50]:
    with open('txt_sentoken_corrected/pos/' + file) as input_file:
        doc = nlp(input_file.read())
        for token in doc:
            list_indice = next((i for i, d in enumerate(term_frequency) if token.lemma_ in d['lemma']), None)
            if not list_indice:
                counter = 1  # / len(doc)
                tmp_dict = {'lemma': token.lemma_, 'counter': counter}
                term_frequency.append(tmp_dict)
            else:
                term_frequency[list_indice]['counter'] = (term_frequency[list_indice]['counter'] + 1)  # / len(doc)
        # term_frequency = sorted(term_frequency, key=lambda i: i['counter'], reverse=True)
        term_frequency_by_txt.append({'text': file, 'term_frequency': term_frequency})

# get words occurences in the whole text corpus
for text in term_frequency_by_txt:
    for word in text['term_frequency']:
        list_indice = next((i for i, d in enumerate(term_frequency_for_all_txt) if word['lemma'] in d['lemma']), None)
        if not list_indice:
            counter = word['counter']
            tmp_dict = {'lemma': word['lemma'], 'counter': counter}
            term_frequency_for_all_txt.append(tmp_dict)
        else:
            term_frequency_for_all_txt[list_indice]['counter'] = term_frequency_for_all_txt[list_indice]['counter'] + word['counter']
term_frequency_for_all_txt = sorted(term_frequency_for_all_txt, key=lambda i: i['counter'], reverse=True)
print(term_frequency_for_all_txt[:5])
