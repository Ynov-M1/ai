#!/usr/bin/env python3
import spacy
import os

nlp = spacy.load('en_core_web_sm')
spacy_stopwords = spacy.lang.en.stop_words.STOP_WORDS
onlyfiles_pos = [name for name in os.listdir('txt_sentoken/pos/') if name[-4:] == ".txt"]
onlyfiles_neg = [name for name in os.listdir('txt_sentoken/neg/') if name[-4:] == ".txt"]

print('starting pos....')
for file in onlyfiles_pos:
    with open('txt_sentoken/pos/' + file, 'r') as input_file:
        doc = nlp(input_file.read())
        with open('txt_sentoken_corrected/pos/' + file[:-4] + '_corrected.txt', 'w+') as output_file:
            tokens = [token for token in doc if token.text not in spacy_stopwords]
            for token in tokens:
                if not token.is_punct:
                    if token.text != ' ':
                        if token.text != '\n':
                            output_file.write(token.text + ' ')


print('starting neg....')
for file in onlyfiles_neg:
    with open('txt_sentoken/neg/' + file, 'r') as input_file:
        doc = nlp(input_file.read())
        with open('txt_sentoken_corrected/neg/' + file[:-4] + '_corrected.txt', 'w+') as output_file:
            tokens = [token for token in doc if token.text not in spacy_stopwords]
            for token in tokens:
                if not token.is_punct:
                    if token.text != ' ':
                        if token.text != '\n':
                            output_file.write(token.text + ' ')
